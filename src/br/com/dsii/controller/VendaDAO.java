/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dsii.controller;

import br.com.dsii.model.Carrinho;
import br.com.dsii.model.Cliente;
import br.com.dsii.model.ItemPedido;
import br.com.dsii.model.Livro;
import br.com.dsii.model.Pedido;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author diegomoraes
 */
public class VendaDAO {

    Connection conexao = null;
    PreparedStatement pst, pst2 = null;
    ResultSet rs, rs2 = null;

    Conexao con = new Conexao();

    //Método para deletar Pedido
    public void deletarPed(Pedido pedido, JFrame jfvenda) {

        String sql = " ";
        String sql2 = " ";

        try {
            conexao = con.conector();
            pst2 = conexao.prepareStatement(sql2);
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, pedido.getPed_id());

            if (JOptionPane.showConfirmDialog(jfvenda, "Deseja deletar?", "Atenção", JOptionPane.YES_NO_CANCEL_OPTION) == 0) {
                pst2.execute();
                pst.execute();
                JOptionPane.showMessageDialog(jfvenda, "Deletado com sucesso!");
                con.desconector(conexao);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfvenda, "Erro ao deletar: " + e);

        }

    }

    //Método para inserir Pedido
    public void inserirPed(Pedido pedido, JFrame jfvenda) {

        String sql = "insert into Pedido (ped_data, ped_cliente, ped_valor) values (?,?,?) ";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setString(1, pedido.getPed_data());
            pst.setInt(2, pedido.getPed_cliente());
            pst.setDouble(3, pedido.getPed_valor());

            pst.execute();

            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, "Erro ao inserir: " + ex);

        }

    }

    //Método para deletar Item pedido
    public void deletarItem(Carrinho car, JFrame jfvenda) {

        String sql = "delete from Carrinho where car_id = ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, car.getCar_id());

            if (JOptionPane.showConfirmDialog(jfvenda, "Deseja deletar?", "Atenção", JOptionPane.YES_NO_CANCEL_OPTION) == 0) {
                pst.execute();
                JOptionPane.showMessageDialog(jfvenda, "Deletado com sucesso!");
                con.desconector(conexao);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfvenda, "Erro ao deletar: " + e);

        }

    }

    //Método para inserir Item Pedido
    public void inserirItem(JFrame jfvenda, int codpedido) {

        String sql = "select * from Carrinho";
        String sql2 = "insert into Item_Pedido (Livro_liv_id, Pedido_ped_id) values (?,?) ";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();
            
           
              while (rs.next()) {

                pst2 = conexao.prepareStatement(sql2);
                pst2.setInt(1, rs.getInt("car_livroid"));
                pst2.setInt(2, codpedido);
                pst2.execute();

            }   
            

           

            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, "Erro ao inserir: " + ex);

        }

    }

    public void consultarItensVenda(JTable tbItens, JFrame jfvenda) {

        String sql = "Select car_id as ID, car_livroid as Codigo, car_livronome as Nome, car_livropreco as Preco from Carrinho";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            tbItens.setModel(DbUtils.resultSetToTableModel(rs));
            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }

    }

    public int consultarPedido(JFrame jfvenda) {

        String sql = "Select * from Pedido";
        int codped=0;
        
        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            if (rs.next()) {
                
                rs.last();
                codped = (rs.getInt("ped_id"))+1;

            }else{
                codped = 1;
            }

            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }
         return codped;
    }

    public void limparCarrinho(JFrame jfvenda) {

        String sql = "Truncate table Carrinho";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.execute();

            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }

    }
    
    public void consultarPedidoFinal(JTable tbConsultaPed, JFrame jfvenda, int codped) {

        String sql = "select itemped_id as ID, liv_nome as Nome, liv_preco as Preco from vw_consultaped where ped_id = ?";

        try {
            conexao = con.conector();
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, codped);
            rs = pst.executeQuery();

            tbConsultaPed.setModel(DbUtils.resultSetToTableModel(rs));
            con.desconector(conexao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(jfvenda, ex);
        }

    }

}

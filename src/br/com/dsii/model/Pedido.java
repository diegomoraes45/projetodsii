/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dsii.model;

/**
 *
 * @author diegomoraes
 */
public class Pedido {

    private int ped_id;
    private String ped_data;
    private int ped_cod;
    private int ped_cliente;
    private double ped_valor;
    
    /**
     * @return the ped_id
     */
    public int getPed_id() {
        return ped_id;
    }

    /**
     * @param ped_id the ped_id to set
     */
    public void setPed_id(int ped_id) {
        this.ped_id = ped_id;
    }

    /**
     * @return the ped_data
     */
    public String getPed_data() {
        return ped_data;
    }

    /**
     * @param ped_data the ped_data to set
     */
    public void setPed_data(String ped_data) {
        this.ped_data = ped_data;
    }

    /**
     * @return the ped_cod
     */
    public int getPed_cod() {
        return ped_cod;
    }

    /**
     * @param ped_cod the ped_cod to set
     */
    public void setPed_cod(int ped_cod) {
        this.ped_cod = ped_cod;
    }

    /**
     * @return the ped_cliente
     */
    public int getPed_cliente() {
        return ped_cliente;
    }

    /**
     * @param ped_cliente the ped_cliente to set
     */
    public void setPed_cliente(int ped_cliente) {
        this.ped_cliente = ped_cliente;
    }

    /**
     * @return the ped_valor
     */
    public double getPed_valor() {
        return ped_valor;
    }

    /**
     * @param ped_valor the ped_valor to set
     */
    public void setPed_valor(double ped_valor) {
        this.ped_valor = ped_valor;
    }
    
    
    
    
    
}

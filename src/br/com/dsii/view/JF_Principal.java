/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dsii.view;

/**
 *
 * @author diegomoraes
 */
public class JF_Principal extends javax.swing.JFrame {

    /**
     * Creates new form JF_Principal
     */
    public JF_Principal() {
        initComponents();
      
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btn_venda = new javax.swing.JButton();
        btn_usuario = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        btn_livro = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Sistema de Venda de Livros");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });

        btn_venda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/if_Basket_877012.png"))); // NOI18N
        btn_venda.setText("Venda");
        btn_venda.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_venda.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_venda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_vendaActionPerformed(evt);
            }
        });

        btn_usuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/if_user-group_285648.png"))); // NOI18N
        btn_usuario.setText("Usuário");
        btn_usuario.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_usuario.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_usuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_usuarioActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/if_kdmconfig_3392.png"))); // NOI18N
        jButton2.setText("Cliente");
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        btn_livro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagens/if_house_shelf_1378832-2.png"))); // NOI18N
        btn_livro.setText("Livros");
        btn_livro.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_livro.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_livro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_livroActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(92, 92, 92)
                .addComponent(btn_usuario)
                .addGap(39, 39, 39)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addComponent(btn_livro)
                .addGap(37, 37, 37)
                .addComponent(btn_venda)
                .addGap(88, 88, 88))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btn_livro)
                    .addComponent(jButton2)
                    .addComponent(btn_usuario)
                    .addComponent(btn_venda))
                .addContainerGap(427, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
        JF_Login login = new JF_Login();
        login.setVisible(true);
    }//GEN-LAST:event_formWindowClosed

    private void btn_usuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_usuarioActionPerformed
        // TODO add your handling code here:
        JF_Usuario jfusuario = new JF_Usuario();
        jfusuario.setVisible(true);
    }//GEN-LAST:event_btn_usuarioActionPerformed

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        // TODO add your handling code here:
         
    }//GEN-LAST:event_formWindowActivated

    private void btn_vendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_vendaActionPerformed
        // TODO add your handling code here:
        JF_Venda jfvenda = new JF_Venda();
        jfvenda.setVisible(true);
    }//GEN-LAST:event_btn_vendaActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        JF_Cliente jfcliente = new JF_Cliente();
        jfcliente.setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btn_livroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_livroActionPerformed
        // TODO add your handling code here:
        JF_Livro jflivro = new JF_Livro();
        jflivro.setVisible(true);
    }//GEN-LAST:event_btn_livroActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JF_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JF_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JF_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JF_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JF_Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_livro;
    private javax.swing.JButton btn_usuario;
    private javax.swing.JButton btn_venda;
    private javax.swing.JButton jButton2;
    // End of variables declaration//GEN-END:variables
}

-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: svl
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.30-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Carrinho`
--

DROP TABLE IF EXISTS `Carrinho`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Carrinho` (
  `car_id` int(11) NOT NULL AUTO_INCREMENT,
  `car_livroid` int(11) NOT NULL,
  `car_livronome` varchar(80) NOT NULL,
  `car_livropreco` double NOT NULL,
  PRIMARY KEY (`car_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Carrinho`
--

LOCK TABLES `Carrinho` WRITE;
/*!40000 ALTER TABLE `Carrinho` DISABLE KEYS */;
/*!40000 ALTER TABLE `Carrinho` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Cliente`
--

DROP TABLE IF EXISTS `Cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Cliente` (
  `cli_id` int(11) NOT NULL AUTO_INCREMENT,
  `cli_nome` varchar(80) DEFAULT NULL,
  `cli_cpf` varchar(20) NOT NULL,
  `cli_data_nasc` varchar(80) NOT NULL,
  PRIMARY KEY (`cli_id`),
  UNIQUE KEY `cli_cpf_UNIQUE` (`cli_cpf`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cliente`
--

LOCK TABLES `Cliente` WRITE;
/*!40000 ALTER TABLE `Cliente` DISABLE KEYS */;
INSERT INTO `Cliente` VALUES (1,'Fulano de Teste 45','11111111123','08/12/1987'),(2,'Diego Moraes','12345654322','08/12/1985');
/*!40000 ALTER TABLE `Cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Item_Pedido`
--

DROP TABLE IF EXISTS `Item_Pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Item_Pedido` (
  `itemped_id` int(11) NOT NULL AUTO_INCREMENT,
  `Livro_liv_id` int(11) NOT NULL,
  `Pedido_ped_id` int(11) NOT NULL,
  PRIMARY KEY (`itemped_id`,`Livro_liv_id`,`Pedido_ped_id`),
  KEY `fk_Livro_has_Pedido_Pedido1_idx` (`Pedido_ped_id`),
  KEY `fk_Livro_has_Pedido_Livro1_idx` (`Livro_liv_id`),
  CONSTRAINT `fk_Livro_has_Pedido_Livro1` FOREIGN KEY (`Livro_liv_id`) REFERENCES `Livro` (`liv_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Livro_has_Pedido_Pedido1` FOREIGN KEY (`Pedido_ped_id`) REFERENCES `Pedido` (`ped_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Item_Pedido`
--

LOCK TABLES `Item_Pedido` WRITE;
/*!40000 ALTER TABLE `Item_Pedido` DISABLE KEYS */;
INSERT INTO `Item_Pedido` VALUES (1,1,5),(2,2,5),(3,2,5),(6,1,9),(7,2,9),(8,2,9),(9,1,9),(10,2,9),(11,2,9),(12,2,14),(13,2,14),(14,1,14),(15,1,14),(16,1,16),(17,2,16),(18,2,16),(19,2,16),(20,1,17),(21,1,17),(22,1,17),(23,2,17),(24,1,18),(25,2,18),(26,1,19),(27,2,19),(28,1,19),(29,1,20),(30,2,20),(31,2,20),(32,1,20),(33,2,20),(34,1,21),(35,2,21),(36,2,21),(37,1,22),(38,1,22),(39,1,22),(40,1,22),(41,1,22),(42,2,22),(43,1,23);
/*!40000 ALTER TABLE `Item_Pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Livro`
--

DROP TABLE IF EXISTS `Livro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Livro` (
  `liv_id` int(11) NOT NULL AUTO_INCREMENT,
  `liv_nome` varchar(100) NOT NULL,
  `liv_custo` double NOT NULL,
  `liv_preco` double NOT NULL,
  `liv_estoque` int(11) NOT NULL,
  PRIMARY KEY (`liv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Livro`
--

LOCK TABLES `Livro` WRITE;
/*!40000 ALTER TABLE `Livro` DISABLE KEYS */;
INSERT INTO `Livro` VALUES (1,'IT A Coisa',30,70,4),(2,'Espera de um Milagre',30,80,3);
/*!40000 ALTER TABLE `Livro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Pedido`
--

DROP TABLE IF EXISTS `Pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido` (
  `ped_id` int(11) NOT NULL AUTO_INCREMENT,
  `ped_data` varchar(20) DEFAULT NULL,
  `ped_cliente` int(11) NOT NULL,
  `ped_valor` double NOT NULL,
  PRIMARY KEY (`ped_id`),
  KEY `fk_cliente_idx` (`ped_cliente`),
  CONSTRAINT `fk_cliente` FOREIGN KEY (`ped_cliente`) REFERENCES `Cliente` (`cli_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Pedido`
--

LOCK TABLES `Pedido` WRITE;
/*!40000 ALTER TABLE `Pedido` DISABLE KEYS */;
INSERT INTO `Pedido` VALUES (1,'28/11/2018',2,0),(2,'28/11/2018',1,0),(3,'29/11/2018',1,0),(4,'28/11/2018',2,0),(5,'28/11/2018',2,0),(6,'28/11/2018',1,0),(8,'28/11/2018',1,0),(9,'28/11/2018',2,0),(10,'28/11/2018',2,0),(13,'',2,0),(14,'28/11/2018',1,0),(15,'28/11/2018',2,0),(16,'29/11/2018',1,0),(17,'28/11/2018',2,0),(18,'29/11/2018',2,0),(19,'22/11/2018',2,220),(20,'22/11/2018',2,380),(21,'29/11/2018',2,230),(22,'29/11/2018',1,430),(23,'29/11/2018',2,70);
/*!40000 ALTER TABLE `Pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Usuario`
--

DROP TABLE IF EXISTS `Usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Usuario` (
  `usu_id` int(11) NOT NULL AUTO_INCREMENT,
  `usu_nome` varchar(80) NOT NULL,
  `usu_login` varchar(20) NOT NULL,
  `usu_senha` varchar(50) DEFAULT NULL,
  `usu_tipo` varchar(30) NOT NULL,
  PRIMARY KEY (`usu_id`),
  UNIQUE KEY `usu_login_UNIQUE` (`usu_login`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Usuario`
--

LOCK TABLES `Usuario` WRITE;
/*!40000 ALTER TABLE `Usuario` DISABLE KEYS */;
INSERT INTO `Usuario` VALUES (1,'Diego Campos','diegomoraes','345','Adm'),(3,'Fulano','fulano','345','Adm'),(4,'Teste2','teste','123','Usu');
/*!40000 ALTER TABLE `Usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `vw_consultaped`
--

DROP TABLE IF EXISTS `vw_consultaped`;
/*!50001 DROP VIEW IF EXISTS `vw_consultaped`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_consultaped` AS SELECT 
 1 AS `itemped_id`,
 1 AS `Livro_liv_id`,
 1 AS `Pedido_ped_id`,
 1 AS `liv_id`,
 1 AS `liv_nome`,
 1 AS `liv_custo`,
 1 AS `liv_preco`,
 1 AS `liv_estoque`,
 1 AS `ped_id`,
 1 AS `ped_data`,
 1 AS `ped_cliente`,
 1 AS `ped_valor`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `vw_consultaped`
--

/*!50001 DROP VIEW IF EXISTS `vw_consultaped`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_consultaped` AS select `I`.`itemped_id` AS `itemped_id`,`I`.`Livro_liv_id` AS `Livro_liv_id`,`I`.`Pedido_ped_id` AS `Pedido_ped_id`,`L`.`liv_id` AS `liv_id`,`L`.`liv_nome` AS `liv_nome`,`L`.`liv_custo` AS `liv_custo`,`L`.`liv_preco` AS `liv_preco`,`L`.`liv_estoque` AS `liv_estoque`,`P`.`ped_id` AS `ped_id`,`P`.`ped_data` AS `ped_data`,`P`.`ped_cliente` AS `ped_cliente`,`P`.`ped_valor` AS `ped_valor` from ((`item_pedido` `I` join `livro` `L`) join `pedido` `P`) where ((`I`.`Pedido_ped_id` = `P`.`ped_id`) and (`I`.`Livro_liv_id` = `L`.`liv_id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-29 21:02:03
